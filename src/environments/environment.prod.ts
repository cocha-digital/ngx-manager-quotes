export const environment = {
  production: true,
  urlSession: 'https://mid.cocha.com/hotels/v1/session',
  getCart: 'https://api.cocha.com/api-manager-quote/products'
};
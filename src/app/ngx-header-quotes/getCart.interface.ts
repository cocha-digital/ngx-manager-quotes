

    export interface TraderData {
        email: string;
        name: string;
        partner: string;
    }

    export interface Channel {
        type: string;
        nemo?: any;
        businessNumber?: any;
        traderData: TraderData;
    }

    export interface Contact {
        documentId: string;
        documentType: string;
        email: string;
        firstLastName: string;
        name: string;
        phoneArea: string;
        phoneCountry: string;
        phoneNumber: string;
        secondLastName: string;
    }

    export interface CheckoutData {
        contact: Contact;
    }

    export interface Group {
        passengerTypes: string;
        passengers: any[];
    }

    export interface ConfirmationData {
        confirmationCode?: any;
    }

    export interface Location {
        city: string;
        code: string;
    }

    export interface Data2 {
        title: string;
        description: string;
        image: string;
        startDate: Date;
        endDate: Date;
        location: Location;
    }

    export interface ImportantToKnow {
        checkInInstructions: string;
        specialCheckInInstructions: string;
        roomFeesDescription: string;
        importantInformation: string;
        mandatoryFeesDescription: string;
    }

    export interface Coordinates {
        latitude: number;
        longitude: number;
    }

    export interface Ratings {
        star: number;
        tripAdvisor: number;
    }

    export interface Details {
        importantToKnow: ImportantToKnow;
        hotelPhone: string;
        hotelName: string;
        address: string;
        coordinates: Coordinates;
        hotelImage: string;
        ratings: Ratings;
        checkInTime: string;
        checkOutTime: string;
        amenities: string[];
        nights: number;
    }

    export interface InfoProductData {
        data: Data2;
        details: Details;
    }

    export interface PrivateData {
        refundable: boolean;
    }

    export interface NightList {
        datePrice: string;
        nightlyPrice: number;
        taxServicePrice: number;
        extraPersonFee: number;
        propertyFee: number;
        taxes: number;
    }

    export interface RoomOccupancy {
        cancellationPolicies: string;
        capacityRoom: string;
        comisionAg: number;
        comisionAs: number;
        fees: any[];
        roomName: string;
        nightList: NightList[];
    }

    export interface Details2 {
        hotelId: string;
        rateType: string;
        roomOccupancy: RoomOccupancy[];
        exchangeRate: number;
    }

    export interface Price {
        total: number;
        base: number;
        taxes: number;
        services: number;
        fees: number;
        discount: number;
        currency: string;
        primary: boolean;
        details: Details2;
    }

    export interface Option {
        value: string;
        label: string;
    }

    export interface Options {
        options: Option[];
        optional: string;
    }

    export interface Requirement {
        type: string;
        title: string;
        id: string;
        value: string;
        options: Options;
    }

    export interface Details3 {
        requirements: Requirement[];
        amenities: string[];
    }

    export interface PricingData {
        privateData: PrivateData;
        prices: Price[];
        details: Details3;
    }

    export interface PrivateData2 {
        trvBookingData?: any;
    }

    export interface PrepurchaseData {
        prepurchaseId: string;
        privateData: PrivateData2;
        refCode: string;
        prices: any[];
    }

    export interface Product {
        _id: string;
        supplier: string;
        type: string;
        groups: Group[];
        confirmationData: ConfirmationData;
        infoProductData: InfoProductData;
        pricingData: PricingData;
        prepurchaseData: PrepurchaseData;
    }

    export interface PaymentDetails {
        charges: any[];
    }

    export interface PaymentData {
        paymentDetails: PaymentDetails;
        paymentId?: any;
    }

    export interface GetCart {
        id: string;
        active: boolean;
        spnr?: any;
        stage: string;
        smartSent: string;
        createDate: Date;
        channel: Channel;
        checkoutData: CheckoutData;
        products: Product[];
        paymentData: PaymentData;
    }

    export interface Data {
        getCart: GetCart;
    }

    export interface GetCartGQLService {
        data: Data;
    }

    export interface Api {
        GetCartGQLService: GetCartGQLService;
    }




import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NgxHeaderQuotesService {


  private cocha_cross  = this.cookieService.get("cocha_cross_p");
  private urlSession = environment.urlSession;
  private urlApi = environment.getCart;

  constructor(  private http: HttpClient,
    private cookieService: CookieService,
  ) { }


  getObtenerRol( ){
    return this.http.get(`${ this.urlSession }/${this.cocha_cross}` );
  }

  getCart( idCart: string ): Observable<any>{
    return this.http.get(`${this.urlApi}/get-cart/${idCart}`)
  }

}

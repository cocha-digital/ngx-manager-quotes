import { Component, OnInit, ViewChild } from '@angular/core';
import { SidenavComponent } from '../sidenav/sidenav.component'
import { NgxHeaderQuotesService } from './ngx-header-quotes.service';
import { Api, PricingData, Product } from './getCart.interface'

@Component({
  selector: 'app-ngx-header-quotes',
  templateUrl: './ngx-header-quotes.component.html',
  styleUrls: ['./ngx-header-quotes.component.css']
})
export class NgxHeaderQuotesComponent implements OnInit {

  sessionUser:any={};
  cartId = localStorage.getItem('idCart') || '61af55d40b66f35cd06fe97b';
  products: Array<Product> = [];
  pricingData: Array<PricingData> = [];
  cantProd: number = 0;

  constructor( private apiServices: NgxHeaderQuotesService ) { 


    this.apiServices.getCart( this.cartId ).subscribe( ( data:Api )  => {
      this.products = data.GetCartGQLService.data.getCart.products;
      this.cantProd = this.products.length;
    });

    this.apiServices.getObtenerRol().subscribe( (data:any)  => {
      this.sessionUser = data;
    });



  }
  // @ViewChild(SidenavComponent)
  // viewProducts!: SidenavComponent;

  @ViewChild(SidenavComponent)
  viewProducts!: SidenavComponent;


  ngOnInit(): void {
  }

  openPreview(){
    this.viewProducts.openPreview();
  }

}

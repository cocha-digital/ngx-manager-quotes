import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgxHeaderQuotesComponent } from './ngx-header-quotes/ngx-header-quotes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './ngx-header-quotes/material.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HttpClientModule } from '@angular/common/http';
import { createCustomElement } from '@angular/elements';


@NgModule({
  declarations: [
    NgxHeaderQuotesComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [NgxHeaderQuotesComponent,NgxHeaderQuotesComponent]
})
export class AppModule { 

  //convirtiendo en web components
  constructor( private injector: Injector ){

    const elementCustom = createCustomElement( NgxHeaderQuotesComponent, { injector: this.injector });
    customElements.define('header-quote', elementCustom);

  }
  ngDoBoostrap(): void{}

}

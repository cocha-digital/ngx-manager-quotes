import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  @Input() products: Array<any> = [];


  opened: boolean = false;
  events: string[] = [];

  openPreview( ){
    this.opened = !this.opened;

    console.log( this.products );

  }

  ngOnInit(): void {
  }

  getHotelRatingImageTag( stairs = 0 ){
    let basePath = 'http://cms.cocha.com/images/rating/';
    let urlImage = basePath + stairs + '.png';
    return urlImage;
  }

  setDate(fecha : any): string{
    let fecha_formato = fecha.substring(0, 10).split("-");
    return  fecha_formato[2] + "/" + fecha_formato[1] + "/" + fecha_formato[0];
  }


  getPrices( prices:any, currency:string ){
    let total = prices.prices.find( ((element:any) => element.currency == currency ))
    return this.priceFormat(total.total, total.currency);
  }


  priceFormat (val:number, currency:string) {
    let currencySymbol;
    if (currency === 'USD') {
        currencySymbol = 'US$';
    } else if (currency === 'CLP') {
        currencySymbol = '$';
    } else {
        currencySymbol = currency;
    }
    if (!isNaN(val)) {
        return currencySymbol + Math.ceil(val).toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&.');
    } else {
        return val;
    }
  }

}

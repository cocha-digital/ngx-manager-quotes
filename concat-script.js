const fs = require('fs-extra');
const concat = require('concat');

build = async () =>{
    const files = [
        './dist/ngx-manager-quotes/runtime.js',
        './dist/ngx-manager-quotes/polyfills.js',
        './dist/ngx-manager-quotes/main.js'
      ];
    
      await fs.ensureDir('widget');
      await concat(files, 'widget/header-quote.js');
}
build();